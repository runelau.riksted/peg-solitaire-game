import tkinter as tk
import numpy as np
import os
import ctypes as Ct
import platform
from concurrent.futures import ThreadPoolExecutor

class Peg():
    '''The Peg class contains the code to analyze a given game board, including calculating invariants and doing the setup, compilation and communication with the C code to solve the game'''
    def __init__(self, input_state:np.array, win_state:np.array):
        
        self.grid_size = input_state.shape[0]
        
        # The game board is defined bellow, 1 is a legal position of a peg, 0 is not an allowed position of a peg. The board must be a square 
        self.input_state = input_state
        self.win_state = win_state
        self.Kill_flag = np.array([0])
        
        self.state = np.array(self.input_state, np.int32)
        self.moves = []
        
        
    @staticmethod
    def invariant(board:np.array, region=False)->(np.array, np.array):
        '''Calculate an invariant for the board state given by 'board', if a region is specified as list of point tuples (calculated with connected_regions), only the invariant for the specified region/points will be calculated. If 2 board states have different invariant, there can be no series of legal moves, which turns one board state in to the other. If the board has several disjointed regions, this has to be true for each region separately.'''
        
        invariant_square = np.zeros((3,3), np.int32)
        move_signature = np.zeros((2,3), int)
        
        if region:
            for n,m in region:
                if board[n,m] == 1:
                    invariant_square[n%3,m%3] += 1
        else: 
            for n in range(0, board.shape[0]):
                for m in range(0, board.shape[0]):
                    if board[n,m] == 1:
                        invariant_square[n%3,m%3] += 1
        
        invariant_square %= 2
        done = False
        
        def flip(axis, index):
            if axis == 0:
                invariant_square[:, index] = (invariant_square[:, index] + 1)%2
                move_signature[0, index] += 1
                
            elif axis == 1:
                invariant_square[index,:] = (invariant_square[index, :] + 1)%2
                move_signature[1, index] += 1
        
        while not done:
            done = True
            for n in range(0,3):
                if np.sum(invariant_square[n,:]) > 1:
                    #flip the row
                    flip(1,n)
                    done = False
        
            for m in range(0,3):
                #flip the column
                if np.sum(invariant_square[:,m]) > 1:
                    flip(0,m)
                    done = False
        
        #the invariant is degenerate when there are 2 or 3 pegs in it, 
        #so we convert it to an unambiguous form (where there are only 2 pegs, which are placed in the corners and/or middle
        if np.sum(invariant_square) >1:
            side_pegs = []
            
            for peg in [(0,1), (2,1), (1,0), (1,2)]:
                if invariant_square[peg] == 1:
                    side_pegs.append( peg )
            
            
            
            if np.sum(invariant_square) == 3 or len(side_pegs) == 1:
                #flips an "I"
                flip(0,1)
                flip(1,0)
                flip(1,2)
            
            for side in side_pegs:
                if side in [(0,1), (2,1)]:
                    flip(1, side[0])
                
                elif side in [(1,0), (1,2)]:
                    flip(0, side[1])
                
        return invariant_square, move_signature%2
    
    @staticmethod
    def connected_regions(board:np.array)->list:
        '''returns a list containing a lists of the points in each connected region.
           A point is considered to be connected to a region if there is a legal move, which involves it and a point in the given region.'''
        
        size = board.shape[0]
        regions = {}
        region_pointers = {}
        region_array = np.zeros( (size, size) , int) - 1
        index = 0
        
        def find_region(point:tuple)->int:
            
            if region_array[point] == -1:
                return -1
                
            a = region_array[point]
            b = region_pointers[a]
            
            while a != b:
                a = b
                b = region_pointers[a]
                
            return b
        
        def allowed_point(point:tuple)->bool:
            return 0 <= point[0] and point[0] < size and 0 <= point[1] and point[1] < size and board[point] != -1
                
            
            
        for x in range(0, size):
            for y in range(0, size):
            
                if board[x,y] != -1:
                    
                    n = find_region((x,y))
                    
                    if n == -1:
                        n = index
                        index += 1
                        regions[n] = [(x,y)]
                        region_array[(x,y)] = n
                        region_pointers[n] = n
                    
                    #for loop over the 4 neighbouring points
                    for d in [(-1,0), (0, -1), (0, 1), (1, 0)]:
                        point = (x + d[0], y + d[1])
                        
                        if allowed_point(point) and (allowed_point( (x + 2*d[0], y + 2*d[1]) ) or  allowed_point( (x - d[0], y - d[1]))):
                            # The point is legal and there is a move that connects the two points.
                            
                            
                            m = find_region(point)
                            
                            if m == -1:
                                #point hasn't been assigned a region yet -> add to region
                                regions[n].append(point)
                                
                            elif m != n:
                                #point has already been asigned to a different region -> merge the regions
                                regions[min(n,m)] +=  regions.pop(max(n,m))
                                region_pointers[max(n,m)] = min(n,m)
                                n = min(n,m)
                                
                            region_array[point] = n

        
        
        return [regions[n] for n in regions]
                                        
         
    def compile_game_board(self):
        '''Checks if a solution can be ruled out by analytical analysis, and does the setup required to run the solution search algorithm written in C'''
        
        solvabillity = ''
        
        self.regions = self.connected_regions(self.state)
        self.move_signature_delta = np.zeros((2,3), int)
        
        for i, region in enumerate(self.regions):
            
            
            input_pegs = 0
            win_pegs = 0
            different = False
            for point in region:
                if self.input_state[point] == 1:
                    input_pegs += 1
                
                if self.win_state[point] == 1:
                    win_pegs += 1
                    
                if self.input_state[point] != self.win_state[point]:
                    different = True
                    
            if win_pegs > input_pegs or (different and ( win_pegs == input_pegs or win_pegs == 0)):
                Solution_impossible = True
                solvabillity = f"A solution is impossible do to the number of pegs in the region containing {point}\n"
                
            
            print(f"Region {i} ( containing {region[0]} ):")
            self.input_invariant, temp = self.invariant(self.input_state, region=region)
            print(f"invariant square of the starting board = \n{self.input_invariant}")
            # signature is modulo 2, so minus and plus are the same
            self.move_signature_delta += temp
            
            self.win_invariant, temp = self.invariant(self.win_state, region=region)
            self.move_signature_delta += temp
            
            print(f"invariant square of the victory board = \n{self.win_invariant}")
            
            if (self.input_invariant != self.win_invariant).any():
                solvabillity = f"The invariant of the starting board and the victory board for the region containing {point} are not identical\n Thus it is mathematically impossible to find a solution\n"
                
        print(solvabillity)
        
        self.move_signature_delta = self.move_signature_delta%2
        if self.move_signature_delta[0][0] %2 == 0:
            self.move_signature_delta = (self.move_signature_delta + 1)%2
        print(f"Move signature delta = \n{self.move_signature_delta}")
    
        self.moves_to_win = np.sum(self.state) - np.sum(self.win_state) # ignoring the -1's in both arrays, because they cancel out.
        
        self.peg_number = 0
        
        for x in range(0,self.grid_size):
            for y in range(0, self.grid_size):
                if self.state[x,y] == 1:
                    self.peg_number +=  1
                    
                    
        self.legal_moves  = []
        
        for n in range(0 , self.grid_size*self.grid_size):
            for d in [0,1,2,3]:
                # peg's position
                n_i = self.index(n)
                
                d_i = [(1,0), (0,1), (-1,0), (0,-1)][d]
                #print(n, n_i, d_i)
                #position of peg to jump over
                m_i = (n_i[0] + d_i[0], n_i[1] + d_i[1])
                # The destination position
                l_i = (m_i[0] + d_i[0], m_i[1] + d_i[1])
                #print(l_i)
                #check whether the destination is outside the board if not continue
                if  l_i[0]>=self.grid_size or l_i[1]>=self.grid_size  or l_i[0]<0 or l_i[1]<0:
                    continue
                
                #print(f"{n_i} = {self.state[n_i]}, {m_i} = {self.state[m_i]}, {l_i} = {self.state[l_i]}")
                #check if the three peg positions are all legal if not continue.
                if self.state[n_i] == -1 or self.state[m_i] == -1 or self.state[l_i] == -1:
                    continue
                    
                #print("is legal")
                self.legal_moves.append([n_i, m_i, l_i, n, d])
                
        self.len_legal_moves = len(self.legal_moves)
        
        self.legal_moves_array = np.array([[self.legal_moves[n][0][0],
                                            self.legal_moves[n][0][1],
                                            self.legal_moves[n][1][0],
                                            self.legal_moves[n][1][1],
                                            self.legal_moves[n][2][0],
                                            self.legal_moves[n][2][1]] for n in range(0, len(self.legal_moves))], np.int32)
        
        self.dependent_moves = np.zeros((len(self.legal_moves_array), 28),np.int32) + len(self.legal_moves)
        
        
        for n in range(0,len(self.legal_moves)):
            c = 0
            #for m in range(len(self.legal_moves)-1, -1, -1):
            for m in range(0, len(self.legal_moves)):
                dependent = False
                for q in [0,1,2]:
                    for p in [0,1,2]:
                        if self.legal_moves[n][q] == self.legal_moves[m][p]:
                            #print(self.legal_moves[n])
                            #print(self.legal_moves[n])
                            #print()
                            self.dependent_moves[n,c] = m
                            c += 1
                            dependent = True
                            break
                    if dependent == True:
                        break
                        
            #if c == 22:
            #    print(self.dependent_moves[n])
        #print(max_c)
        #import sys
        #np.set_printoptions(threshold=sys.maxsize)
        #print(self.dependent_moves)
        return solvabillity
    
    
    has_checked_compiler = False
    
    @classmethod           
    def check_compiler(cls, gcc_failed=False):
        import subprocess
        
        # checking if gcc is available"
        
        gcc_available = False
        try:
            subprocess.run(["gcc", "--version"], stdout=subprocess.DEVNULL)
            gcc_available = True
        except FileNotFoundError as e:
            pass
            
        if gcc_available and not gcc_failed:
            
            print("gcc compiler available to compile optimized code :D")
            if not os.path.isdir("__temp__"):
                os.mkdir("__temp__")
            cls.compile_c_code = cls.compile_gcc
            
            cls.has_checked_compiler = True
            
        else:
            # Try to use a precompile shared object independent of the values of grid_size, number of pegs, number of legal_moves and so on.
            print("gcc not available")
            try:
                library = os.path.join("Fallback_binaries", f"peg_{platform.system()}_{platform.architecture()[0]}.so")
                cls.precompiled_lib = Ct.cdll.LoadLibrary(os.path.join(os.getcwd(), library))
                cls.compile_c_code = cls.use_precompiled_fallback
                cls.precompiled_lib.set_up_globals.restype = Ct.c_int
                cls.precompiled_lib.solve.restype = Ct.c_int
                
                cls.has_checked_compiler = True
                
            except Exception as e:
                print(e)
                print(f"Tried to load a precompiled version of peg_general.c, but could not load \"{library}\"")
                print(f"Please compile \"peg_general.c\" into a shared object named \"{library}\"")
                return f"Tried to load a precompiled version of peg_general.c\n but could not load \"{library}\""
        
        return 0
    
    def compile_c_code(self, compile_num):
        '''self.check_compiler will try to replace this function with the appropriate function'''
        raise(NotImplementedError)
        return 3
    
    def compile_gcc(self, compile_num):
        Compile_definitions = {     "general":      1,
                                    "pegs":         self.peg_number,
                                    "legal_moves":  len(self.legal_moves),
                                    "grid_size":    self.grid_size,
                                    "moves_to_win": self.moves_to_win}
        
        Compile_instruction = f'gcc -shared -fPIC -o {os.path.join(os.getcwd(), "__temp__",  f"peg_temp{compile_num}.so")} peg.c'
        for key in Compile_definitions:
            Compile_instruction += f" -D{key}={Compile_definitions[key]}"
        
        compile_response = os.system(Compile_instruction)
        
        if(compile_response):
            print(f"Compile error\ntried to compile with the instructions:\n{Compile_instruction}\n Trying fallback")
            self.check_compiler(gcc_failed = True)
            self.compile_c_code(compile_num)
        #else:
            #print(f"Compiled with the compiler definitions:\n {Compile_definitions}")
            
        
        
        
        lib = Ct.cdll.LoadLibrary(os.path.join(os.getcwd(), "__temp__",  f"peg_temp{compile_num}.so"))
        solve = lib.solve
        solve.restype = Ct.c_int
        return lambda move_hist:  solve(  Ct.c_void_p(self.state.ctypes.data), 
                                Ct.c_void_p(self.win_state.ctypes.data), 
                                Ct.c_void_p(self.legal_moves_array.ctypes.data),
                                Ct.c_void_p(move_hist.ctypes.data),
                                Ct.c_void_p(self.dependent_moves.ctypes.data),
                                Ct.c_void_p(self.Kill_flag.ctypes.data)
                                )
                                
    def use_precompiled_fallback(self, compile_num):
        set_up = self.precompiled_lib.set_up_globals(   Ct.c_int(self.grid_size),
                                                        Ct.c_int(self.peg_number),
                                                        Ct.c_int(len(self.legal_moves)),
                                                        Ct.c_int(self.moves_to_win), 
                                                        Ct.c_void_p(self.legal_moves_array.ctypes.data),
                                                        Ct.c_void_p(self.dependent_moves.ctypes.data)
                                                        )
        if set_up:
            print(f"The fallback precompiled shared object failed in function set up, code ={set_up}")
            raise(NotImplementedError)
            return 3
        
        
        return lambda move_hist: self.precompiled_lib.solve(    Ct.c_void_p(self.state.ctypes.data), 
                                                                Ct.c_void_p(self.win_state.ctypes.data),
                                                                Ct.c_void_p(move_hist.ctypes.data),
                                                                Ct.c_void_p(self.Kill_flag.ctypes.data)
                                                                ) 
            
        
    def solve_c(self, compile_num)-> str:
        print("\nrunning solve_c")
        solvabillity = self.compile_game_board()
        #print("Trying to solve the following game:")
        #print(self.state)
        
        if solvabillity:
            
            return solvabillity
        
        if self.has_checked_compiler == False:
            response = self.check_compiler()
            if response:
                return response
            
        
        move_hist = np.zeros(self.peg_number, np.int32) - 1
        for i, move in enumerate(self.moves):
            move_hist[i] = move
        
        
        self.Kill_flag = np.array([0])
        solve = self.compile_c_code(compile_num)
        exit_code = solve(move_hist)
        
        print(f"exit code from C = {exit_code}")    
        
        if exit_code == 0:
            solvabillity = "Solution found"
            
            move_signature = np.zeros((2,3), int)
            
            for n in range(0,self.peg_number):
                if move_hist[n] != -1:
                    self.moves.append(move_hist[n]);
                    move = self.legal_moves[move_hist[n]]
                    
                    move_signature[ move[4]%2, move[0][(move[4]+1)%2]%3 ] += 1
                else:
                    break
            
            
            if move_signature[0][0] %2 == 0:
                move_signature = move_signature + 1
            move_signature = move_signature%2
            
            
            print(f"move sequence invariant:\n{move_signature}")
                    
            if False:
                print(self)
                for q in range(0, len(self.moves)):
                    print(f" move {self.moves[q]} = {self.move_legal(self.moves[q])}")
                    print(self)
        elif exit_code == 1:
            solvabillity = "Searched all possible move combinations, but no solution was found."
        elif exit_code == 2:
            solvabillity = "Terminated"
        
        return solvabillity
    
    
        
    def move(self, q):
        move = self.legal_moves[q]
        if self.state[move[0]] == 1 and self.state[move[1]] == 1 and self.state[move[2]] == 0:
            self.state[move[0]] = 0
            self.state[move[1]] = 0
            self.state[move[2]] = 1
            self.moves.append(q)
            #print('Moving')
            return 0
        else:
            return 1
            
        
    def back_track(self):
        n_i, m_i, l_i, _, _ = self.legal_moves[self.moves[-1]]
        
        #print(f'Backtrack, move=({n_i},  {d_i})')
        self.state[n_i[0], n_i[1]] = 1
        self.state[m_i[0], m_i[1]] = 1
        self.state[l_i[0], l_i[1]] = 0
        
        return self.moves.pop(-1)
        
        
    def index(self, n):
        # returns the grid coordinates of the n'th peg position.
        return (n//self.grid_size, n%self.grid_size)
        
    def __str__(self):
        temp = np.array(self.state)
        if (self.moves):
            temp[self.legal_moves[self.moves[-1]][0]] = 3
            temp[self.legal_moves[self.moves[-1]][1]] = 3
            temp[self.legal_moves[self.moves[-1]][2]] = 2
        return str(temp)
    
    def kill(self):
        self.Kill_flag[0] = 1

class NumEntry(tk.Entry):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.bind("<Key>", self.key_press)
        self.bind("<Control-a>", self.crtl_a )
        
    
    def key_press(self, event):
        
        #print(event)

        char = repr(event.char)[1:-1]
        # print('char={}, len={}'.format(char, len(char)))
        if len(char) == 1:
            if char in '1234567890':
                return
            else:
                return 'break'
    
    
    def crtl_a(self, *event):
        self.select_range(0,tk.END)
        return 'break'

class LoadingIndicator(tk.Label):
    
    
    start_delay = 1000 #miliseconds
    refresh_time = 1500 #miliseconds
    
    def __init__(self, master, row=None, column= None, *args, **kwargs):
        super().__init__(master, *args, *kwargs)
        self.state = 0
        self.column = column
        self.row = row
        self.after_id = None
        self.loading_images = [ tk.PhotoImage(file=os.path.join("Graphics", "loading0.png"), master=master),
                                tk.PhotoImage(file=os.path.join("Graphics", "loading1.png"), master=master),
                                tk.PhotoImage(file=os.path.join("Graphics", "loading2.png"), master=master),
                                tk.PhotoImage(file=os.path.join("Graphics", "loading3.png"), master=master)]
        
        self.configure(image=self.loading_images[self.state])
        
    def start(self, delay = True):
    
        if delay == True:
            delay = self.start_delay
        
        if type(delay) == int and delay != 0:
            self.after_id = self.after(delay, self.start, 0 )
            return
        else:
            if self.after_id:
                self.after_cancel(self.after_id)
        
        if self.column != None and self.row != None:
            self.grid(column=self.column, row=self.row)
        else:
            self.grid()
            
        self.next()
        
    def next(self):
        self.state = (self.state + 1)%4
        self.configure(image=self.loading_images[self.state])
        self.after_id = self.after(self.refresh_time, self.next)
    
    def stop(self):
        if self.after_id:
            self.after_cancel(self.after_id)
        self.grid_remove()
        
        
class DummyLabels():
    ''' This does nothing. Stand in for a Label or a list of tk.Label-s, but when called or grided it simply does nothing. Intended to stand in for a list of blank labels.'''
    
    def __init__(self, *args, **kwargs):
        self.x = None
        self.y = None
        self.typ = "blank"
        
    def pop(self, *args, **kwargs):
        return self 
    
    def __getitem__(self, *args, **kwargs):
        return self
    
    def __getattr__(self, *args, **kwargs):
        return self
    
    def __call__(self, *args, **kwargs):
        return None
        
class SolitaireBoard(tk.Frame):
    
    def __init__(self, root, grid_size, *args, **kwargs):
        self.grid_size = grid_size
        self.owner = kwargs.pop("owner", None)
        self.interactive = kwargs.pop("interactive", True)
        
        self.field_height = 42
        self.field_width = 50
        
        super().__init__(root, *args, **kwargs)
        self.Labels = {"peg":[], "hole":[], "blank":DummyLabels()}
        self.Label_state = []
        self.state = []
        
        self.type_2_code = {"peg":1, "hole":0, "blank":-1}
        self.code_2_type    = {self.type_2_code[typ]:typ for typ in self.type_2_code}
        self.images = {"peg": tk.PhotoImage(file=os.path.join("Graphics", "peg.png"), master=root),
                       "hole": tk.PhotoImage(file=os.path.join("Graphics", "hole.png"), master=root)
                       }
                       
       
        
        
        self.bind("<Button>", self.toggle_label)
        for x in range(0, self.grid_size):

            for typ in self.Labels:
                self.Labels[typ].append([])
            self.state.append([])
            
            for y in range(0, self.grid_size):
                self.add_cell(x,y, default_type="peg")
                
        for n in range(0, self.grid_size):
            self.columnconfigure(n, weight=1, minsize = self.field_width)
            self.rowconfigure(n, weight=1, minsize = self.field_height, pad=2)
        
    def add_cell(self, x,y, default_type="hole", restore=False):
        
        if restore and len(self.state) > x and len(self.state[x]) > y:
            default_type = self.code_2_type[self.state[x][y]]
        else:
            self.state[x].append(self.type_2_code[default_type])
            
        for typ in ["peg", "hole"]:
            self.Labels[typ][x].append(tk.Label(self, image=self.images[typ]))
            self.Labels[typ][x][y].x = x
            self.Labels[typ][x][y].y = y
            self.Labels[typ][x][y].typ = typ
            self.Labels[typ][x][y].bind("<Button>", self.toggle_label)
        
        self.Labels[default_type][x][y].grid(row = x, column = y)
        
    
    def toggle_label(self, event):
        if not self.interactive:
            return
            
        # Determine which field on the board was clicked, and get the corresponding active label.
        if event.widget is self:
            
            x = (event.y*self.grid_size)//self.winfo_height()
            y = (event.x*self.grid_size)//self.winfo_width()
            typ = self.code_2_type[ self.state[x][y] ] 
            label = self.Labels[typ][x][y]
        else:
            label = event.widget
            x, y, typ = label.x, label.y, label.typ
        
        
        if event.num == 1:
                if typ == "peg":
                    target = "hole"
                elif typ == "hole":
                    target = "peg"
                else:
                    return
            
        elif event.num == 3:
            if typ == "hole" or typ == "peg":
                self.owner.change_blanks(x,y,"blank")
                return 
            elif typ == "blank":
                self.owner.change_blanks(x,y,"hole")
                return 
        else:
            return 
        
        label.grid_remove()
        self.Labels[target][x][y].grid(row=x, column=y)
        self.state[x][y] = self.type_2_code[target]
    
    def set_state(self, state_list:list):
        
        
        if type(state_list) != list:
            try:
                state_list = [[ item for item in row] for row in state_list]
            except TypeError:
                print("state_list has to be a list of lists, or a 2D numpy array")
                return
        
        
        for x in range(0, self.grid_size):
            for y in range(0,self.grid_size):
                if self.state[x][y] != state_list[x][y]:
                    self.Labels[self.code_2_type[self.state[x][y]]][x][y].grid_remove()
                    self.Labels[self.code_2_type[state_list[x][y]]][x][y].grid(row = x, column=y)
        
        self.state = state_list
        
        
        
    def set_grid_size(self, size):
        
        if size > self.grid_size:
            
            for x in range(0, self.grid_size):
                for y in range(self.grid_size, size):
                    self.add_cell(x,y, restore=True)
                
            for x in range(self.grid_size, size):
                for typ in self.Labels:
                    self.Labels[typ].append([])
                if len(self.state) >= x:
                    self.state.append([])
                
                for y in range(0, size):
                    self.add_cell(x,y, restore=True)
            
            for n in range(self.grid_size, size):
                self.columnconfigure(n, weight=1, minsize = self.field_width)
                self.rowconfigure(n, weight=1, minsize = self.field_height, pad = 2)
            
            self.grid_size = size
                
        elif size < self.grid_size:
            
            for typ in self.Labels:
                for x in range(0, size):
                    for y in range(size, self.grid_size):
                        self.Labels[typ][x].pop(size).destroy()
                
                for x in range(size, self.grid_size):
                    for y in range(0, self.grid_size):
                        self.Labels[typ][size].pop(0).destroy()
                    self.Labels[typ].pop(size)
            
            for n in range(size, self.grid_size):
                self.columnconfigure(n, weight=0, minsize = 0)
                self.rowconfigure(n, weight=0, minsize = 0, pad =0)
        
        self.grid_size = size
    
    def get_state_array(self):
        return np.array([[self.state[x][y] for y in range(0,self.grid_size)] for x in range(0,self.grid_size) ], np.int32)
                        
class MainFrame(tk.Frame):
    
    def __init__(self, root, *args, **kwargs):
        super().__init__(root, *args, **kwargs)
        self.root = root
        self.root.protocol("WM_DELETE_WINDOW", self.destroy)
        self.root.title("Peg Solitaire Solver")
        self.boards = []
        self.executor = None
        self.Initial_label = tk.Label(self, text="Choose\nstarting board:",font=("Arial",  self.root.DPI_scale(20)))
        self.Initial_label.grid(row=2, column=1)
        self.Initial_board = SolitaireBoard(self, 7, owner=self)
        self.boards.append(self.Initial_board)
        self.Initial_board.grid(row=3, column = 1)
        
        self.Arrow = tk.Label(self, text=" ➪ ", font=("Arial",  self.root.DPI_scale(30)))
        self.Arrow.default = lambda: self.Arrow.configure(text=" ➪ ")
        
        self.Arrow.grid(row=3, column=2)
        
        
                
       
        self.Loading_indicator = LoadingIndicator(self, 3,2)
        
        
        
        
        self.Win_label = tk.Label(self, text="Define\nvictory condition:",font=("Arial",  self.root.DPI_scale(20)))
        self.Win_label.grid(row=2, column=3)
        self.Win_board = SolitaireBoard(self, 7, owner=self, pady=self.root.DPI_scale(10))
        self.Win_board.grid(row=3, column = 3)
        self.boards.append(self.Win_board)
        
        
        self.Entry_label = tk.Label(self, text="Board size:",font=("Arial",  self.root.DPI_scale(20)))
        self.Entry_label.grid(row=1, column =2)
        self.Grid_size_entry = NumEntry(self, width= 2, justify=tk.CENTER, font=("Arial",  self.root.DPI_scale(20)))
        self.Grid_size_entry.grid(row =2, column=2)
        self.Grid_size_entry.insert(0, 7)
        self.Grid_size_entry.bind("<Return>", self.set_grid_sizes)
        self.bootom_label = tk.Label(self, text="Left click to switch between holes and pegs\n Right click to add or remove a field from the game board", pady=self.root.DPI_scale(10), font=("Arial", self.root.DPI_scale(12) ) )
        self.bootom_label.default_text = self.bootom_label["text"]
        self.bootom_label.grid(row=4, column=1, columnspan=3)
        
        self.Solve_button = tk.Button(self)
        self.Solve_button = tk.Button(self, text="Solve", command=self.solve,font=("Arial",  self.root.DPI_scale(20)))
        self.Solve_button.grid(row=5, column= 2)
        
        self.next_button = tk.Button(self, text = "⇨", font=("Arial", self.root.DPI_scale(20), "bold"))
        self.back_button = tk.Button(self, text = "⇦", font=("Arial", self.root.DPI_scale(20), "bold"))
        
        self.Initial_board.set_state([  [-1,-1, 1,1,1,-1,-1],
                                        [-1,-1, 1,1,1,-1,-1],
                                        [ 1, 1, 1,1,1, 1, 1],
                                        [ 1, 1, 1,0,1, 1, 1],
                                        [ 1, 1, 1,1,1, 1, 1],
                                        [-1,-1, 1,1,1,-1,-1],
                                        [-1,-1, 1,1,1,-1,-1]])
                                        
        self.Win_board.set_state([      [-1,-1, 0,0,0,-1,-1],
                                        [-1,-1, 0,0,0,-1,-1],
                                        [ 0, 0, 0,0,0, 0, 0],
                                        [ 0, 0, 0,1,0, 0, 0],
                                        [ 0, 0, 0,0,0, 0, 0],
                                        [-1,-1, 0,0,0,-1,-1],
                                        [-1,-1, 0,0,0,-1,-1]])
                                        
        
        
        self.compile_number =0
        self.rowconfigure(1, {'minsize': 70})
        self.rowconfigure(2, {'minsize': 70})
        self.rowconfigure(4, {'minsize': 70})
        
        
        
    def set_grid_sizes(self, *event):
        try:
            size = int(self.Grid_size_entry.get())
        except ValueError:      
            return
        
        for board in self.boards:
            board.set_grid_size(size)
    
    
    def change_blanks(self, x,y, target):
        for board in self.boards:
            for typ in board.Labels:
                board.Labels[typ][x][y].grid_remove()
            board.Labels[target][x][y].grid(row=x, column=y,)
            board.state[x][y] = board.type_2_code[target]
            
            
    def color_connected_regions(self):
        '''A function to test the regions created by Peg.connected_regions'''
        colors = ["red", "blue", "green", "yellow", "gray", "orange", "cyan"]
        i = 0
        for cls in self.peg.connected_regions(self.Initial_board.get_state_array()):
            for point in cls:
                
                for typ in ["peg", "hole"]:
                    self.Initial_board.Labels[typ][point[0]][point[1]].configure(bg=colors[i])
            i = (i+1) % len(colors)
        
        
    def solve(self):
        
        for board in self.boards:
            board.interactive = False
        
        self.Grid_size_entry.configure(state=tk.DISABLED)
        self.peg = Peg( self.Initial_board.get_state_array(), self.Win_board.get_state_array() )
        #self.color_connected_regions()
        
        
        self.Solve_button.configure(text="Reset", command=self.reset)
        self.bootom_label.configure(text="Looking for a solution...")
        self.Loading_indicator.start()
        #self.Arrow.grid_remove()
        
        if not self.executor:
            self.executor = ThreadPoolExecutor(1)
        
        self.future = self.executor.submit(self.peg.solve_c, self.compile_number)
        self.future.add_done_callback(self.show_solution)
        self.compile_number += 1
        
        return
        
        
    def show_solution(self, *future):
        
        
        solvabillity = self.future.result()
        
        self.Loading_indicator.stop()
        self.Arrow.grid()
        
        if solvabillity == "Solution found":
            self.move_number = 0
            self.bootom_label.configure(text="Displaying Solution.\nUse the arrows to display the next or previous move in the solution.")
            def next_move(*event):
                if self.move_number == len(move_list):
                    return
                #print("Displaying next move")
                self.Initial_board.set_state(self.peg.state)
                self.peg.move(move_list[self.move_number])
                self.Win_board.set_state(self.peg.state)
                self.move_number +=1
                self.Arrow.configure(text=f"move\n{self.move_number}\n ➪ \n\n ")
                
                #print(self.Win_board.get_state_array())
                if self.move_number == len(move_list):
                    self.next_button.configure(state=tk.DISABLED)
                elif self.move_number == 2:
                    self.back_button.configure(state=tk.NORMAL)
            
            def previous_move(*event):
                if self.move_number == 1:
                    return
                #print("Displaying previous move")
                self.peg.back_track()
                
                self.Win_board.set_state(self.peg.state)
                self.peg.back_track()
                self.move_number -=1
                self.Initial_board.set_state(self.peg.state)
                self.Arrow.configure(text=f"move\n{self.move_number}\n ➪ \n\n ")
                #print(self.Win_board.get_state_array())
                
                self.peg.move(move_list[self.move_number-1])
                
                if self.move_number == 1:
                    self.back_button.configure(state=tk.DISABLED)
                elif self.move_number == len(move_list) - 1:
                    self.next_button.configure(state=tk.NORMAL)
            
            
            move_list = list(self.peg.moves)
            
            self.peg.moves = []
            self.peg.state = self.Initial_board.get_state_array()
            self.Win_label.grid_remove()
            self.Initial_label.grid_remove()
            self.next_button.grid(row=5, column=3)
            self.back_button.grid(row=5, column=1)
            self.next_button.configure(command=next_move)
            self.next_button.configure(state=tk.NORMAL)
            self.back_button.configure(state=tk.DISABLED, command=previous_move)
            self.next_button.binding = self.root.bind("<Right>", next_move)
            self.back_button.binding = self.root.bind("<Left>", previous_move)
            next_move()
        
        elif solvabillity == "Terminated":
            print("C Code was terminated")  
        else:
            self.bootom_label.configure(text=solvabillity)
        
    def reset(self):
        self.peg.kill() 
        
        try:
            self.excecutor.shutdown()
        except AttributeError:
            pass
        
        self.Loading_indicator.stop()
        self.Initial_label.grid()
        self.Grid_size_entry.configure(state=tk.NORMAL)
        self.Initial_board.set_state(self.peg.input_state)
        self.Win_board.set_state(self.peg.win_state)
        
        
        
        self.Win_label.grid()
        self.Arrow.default()
        
        self.Entry_label.grid()
        self.Grid_size_entry.grid()
        self.bootom_label.configure( text=self.bootom_label.default_text)
        
        
        self.Solve_button.configure(text="Solve", command=self.solve)
        try:
            self.next_button.grid_remove()
            self.back_button.grid_remove()
            self.next_button._tclCommands.pop(0)
            self.back_button._tclCommands.pop(0)
            self.root.unbind("<Right>", self.next_button.binding)
            self.root.unbind("<Left>", self.back_button.binding)
        except AttributeError:
            pass
        except IndexError:
            pass
        except _tkinter.TclError:
            pass
        
        for board in self.boards:
            board.interactive = True
    
    def destroy(self):
        
        
        try:
            self.peg.kill() 
        except AttributeError as e:
            pass
        except Exception as e:
            print(e)
            print("proceeding with closure")
        
        if os.path.isdir("__temp__"):
            for file in os.listdir("__temp__"):
                if "peg_temp" in file:
                    os.remove(os.path.join("__temp__", file))
        
        #ther seems to be a problem, when the peg c code is running, so we close, that and then give python a moment to clean up before, we as tk to close.
        self.after(100, self.root.quit)
            
        
if __name__ == "__main__":
    Root = tk.Tk()

    try:
        import DPIscaling
        DPIscaling.make_Tk_DPI_aware(Root)
    except ModuleNotFoundError:
        Root.DPI_scaling = 1
        Root.DPI_geometry = lambda s: s
        Root.DPI_scale = lambda s: round(s)
        
    Root.tk.call('wm', 'iconphoto', Root._w, tk.PhotoImage(file=os.path.join("Graphics", 'peg_solitaire_logo.png')))
        
    Boards_frame = MainFrame(Root)
    Boards_frame.pack()
    Root.mainloop()
