import peg
import numpy as np


def flip(invariant_square, axis, index):

    if axis == 0:
        invariant_square[:, index] = (invariant_square[:, index] + 1)%2
        
    elif axis == 1:
        invariant_square[index,:] = (invariant_square[index, :] + 1)%2
                    
zero = np.zeros((3,3))
print("running")
if 0:
    for n in range(0,65):
        print(f"\n{n}")
        a = np.zeros((3,3))
        c = n
        for m in range(0,6):
            if c%2:
                flip(a, m//3, m%3)
                print(f"flipping ({m//3}, {m%3})")
            c = c//2
        
        
        #print(a)
        a,_ = peg.Peg.invariant(a)
        if (a != zero).any():
            print(a)
            print(c)


eq_classes = []
 
for n in range(0,2**9):
    c =n
    a = np.zeros((3,3), int)
    found = False
    for m in range(0, 9):
        if c%2:
            a[m//3, m%3] = 1
        c = c//2
    
    for cls in eq_classes:
        b = (cls[0] + a)%2
        b,_ = peg.Peg.invariant(b)
        if (b == zero).all():
            cls.append(a)
            found = True
    
    if not found:
        
        eq_classes.append([peg.Peg.invariant(a)[0], a])




for n, cls in enumerate(eq_classes):
    print(f"\n\n class {n} len={len(cls)-1}+1")
    print(f"{cls[0]}")
#print(eq_classes)


def check_all():

    print("\ninvariant fails:")
    q = 0
    for n in range(0,2**9):
        c =n
        a = np.zeros((3,3))
        for m in range(0, 9):
            if c%2:
                a[m//3, m%3] = 1
            c = c//2
        
        found = 0
        b,_ = peg.Peg.invariant(a)
        for cls in eq_classes:
            if (cls[0] == b).all():
                found += 1
        
        if found == 0:
            print(f"\n{found}\n{a}\n{b}")
            q += 1
        elif found > 1:
            print(f"\n{found}\n{a}\n{b} not unique")
            


    print(q)
        
    
            
                    
                    
if __name__ == "__main__":
    check_all()
                    
                    
                    
