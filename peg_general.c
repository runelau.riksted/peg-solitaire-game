#include <string.h>
#include <stdio.h>

#define old 0

//Un comment the line bellow if you want to compile peg_general.c in to an object (i.e. if you want to do the setup manually, and not use peg.py)
#define general 1

#if 0
    #define legal_moves 92
    #define pegs 36
    #define grid_size 7
    #define moves_to_win pegs-1
#endif

int legal_moves;
int pegs;
int grid_size;
int moves_to_win;

int counter = 0;
int move_num = 0;
int (*moves)[6];
int *move_hist;
int (*dependent)[28];
int *dependent_len;


int can_move(const int q, const int state[grid_size][grid_size]){
    //for(int i = 0; i <6; i++){
    //    printf("if = %d\t%d\t%d\n", state[moves[q][0]][moves[q][1]], state[moves[q][2]][moves[q][3]], state[moves[q][4]][moves[q][5]]);
    //}
    return state[moves[q][0]][moves[q][1]] == 1 
        && state[moves[q][2]][moves[q][3]] == 1 
        && state[moves[q][4]][moves[q][5]] == 0;

}


void back_track(const int q, int state[grid_size][grid_size]){
    //printf("backtracking\n");
    //for(int i = 0; i <6; i++){
    //    printf("moves[%d][%d] = %d, \t", 31, i, moves[q][i]);
    //}
    state[moves[q][0]][moves[q][1]] = 1;
    state[moves[q][2]][moves[q][3]] = 1;
    state[moves[q][4]][moves[q][5]] = 0;
    move_num --;

}


int not_done( const int state[grid_size][grid_size], const int win_board[grid_size][grid_size]){
    if (move_num != moves_to_win){
        return 1;
    }
    for(int n= 0; n< grid_size; n++){
        for(int m= 0; m< grid_size; m++){
            if(state[n][m] != win_board[n][m] && state[n][m] != -1 && win_board[n][m] != -1){
                return 1;
            }
        }
    }
    
    return 0;
}

void print_state( const int state[grid_size][grid_size]){
    for(int n= 0; n< grid_size; n++){
        
        for(int m= 0; m< grid_size; m++){
            printf("%2d  ", state[n][m]);
        }
        printf("\n");
    }
}

int not_a_permutation(const int q, const int move_hist[moves_to_win], const short are_dependent_m[legal_moves][legal_moves]){
    
    for(int i =move_num-1; i>= 0; i--){
        if( are_dependent_m[q][move_hist[i]] ){
            //They don't commute i.e. it can't be a permutation of a previous path.
            return 1;
        }else{
            //q and move_hist[i] are independent i.e. they commute
            if(q < move_hist[i]){
                //printf("q=%d is a permutation with %d\n", q, move_hist[i]);
                return 0;
            }
        }
    }
    return 1;
}

void compile_dependent(const int dependent[legal_moves][28], short are_dependent_m[legal_moves][legal_moves]){
    memset(are_dependent_m, 0, sizeof(short)*legal_moves*legal_moves);
    int c;
    for (int q=0; q<legal_moves; q++){
        are_dependent_m[q][q] = 1;
        c = 0;
        for (int p=0; p< 28; p++){
        
            if(dependent[q][p] == legal_moves){
                break;
            }
            c++;
            are_dependent_m[q][dependent[q][p]] = 1;
            are_dependent_m[dependent[q][p]][q] = 1;
        }
        dependent_len[q] = c;
    
    }

}


typedef struct list{
    int move;
    struct list** next;

}list;

list *move_list_array;
list **move_list;


void update_move_list(const int Q, const int state[grid_size][grid_size]){
    
    const int index = move_num -1;
    const int new_index = move_num;
    
    list *entry = move_list[index];
    list **new_list_end = &move_list[new_index];
    
    //int reached_end 0;
    int p = 0;
    //printf("update_move_list Q = %d\n", Q);
    int q;
    
    void append_at_end(){
        //If we are at the end of the list (*entry = NULL) we can do the updating more efficiently.
        
        for(p; p < dependent_len[Q]; p++){
            q = dependent[Q][p];
            
            if(can_move(q, state)){
                    
                *new_list_end = &move_list_array[q];
                new_list_end = &(move_list_array[q].next[new_index]);
            }
        }
        
        *new_list_end = NULL;
        return;

    }
    
    
    for(p=0; p < dependent_len[Q]; p++){
        
        q = dependent[Q][p];
        //printf("q=%d\t entry->move = %d\n", q, entry->move );
        
        while(/**entry != NULL*/entry->move < q){
            *new_list_end = entry;
            new_list_end = &(entry->next[new_index]); 
            entry = entry->next[index];
            if(entry == NULL){
                append_at_end();
                return ;
            }
            
            
            //printf("-> %d\n", entry->move);
        }
        
        
        if(q == entry->move){
            //printf("N01 %d\n", q);
            entry = entry->next[index];
            if(entry == NULL){
                append_at_end();
                return ;
            }
            
            //entry = &move_list;
        }else{
        
            if(can_move(q, state)){
                //printf("YES\n");
                
                *new_list_end = &move_list_array[q];
                new_list_end = &(move_list_array[q].next[new_index]);
                
                if(entry == NULL){
                    append_at_end();
                    return ;
                }
            }/*else{
                printf("NO2\n");
            }*/
        }
    }
    
    *new_list_end = entry;
    
    while(entry != NULL){
        
        entry = entry->next[new_index] = entry->next[index];
    }
    //*new_list_end = NULL;
    
}

void move(const int q, int state[grid_size][grid_size], int move_hist[moves_to_win]){
    //for(int i = 0; i <6; i++){
    //    printf("if = %d\t%d\t%d\n", state[moves[q][0]][moves[q][1]], state[moves[q][2]][moves[q][3]], state[moves[q][4]][moves[q][5]]);
    //}
    
    state[moves[q][0]][moves[q][1]] = 0;
    state[moves[q][2]][moves[q][3]] = 0;
    state[moves[q][4]][moves[q][5]] = 1;
    move_hist[move_num] = q;
    move_num ++;
    #if old == 0
    update_move_list(q, state);
    #endif

}

void init_move_list(int index, const int state[grid_size][grid_size], list* next_pointers[legal_moves][moves_to_win]){
    
    list **next_node = &move_list[index];
    
    for(int q=0; q < legal_moves; q++){
        move_list_array[q].move = q;
        move_list_array[q].next = &next_pointers[q][0];
        if(can_move(q, state)){
            //printf("q=%d\n", q);
            *next_node = &move_list_array[q];
            next_node = &(move_list_array[q].next[index]);
        }
    }
        
    *next_node = NULL;
    
    /*for(list *ptr = move_list[index]; ptr != NULL;  ptr = ptr->next[index]){
        printf("%d->\n", ptr->move);
    
    }*/
    return;

}


int set_up_globals(const int _grid_size, const int _pegs, const int _legal_moves, const int _moves_to_win,  int _moves[_legal_moves][6],  int _dependent[_legal_moves][28]){
    
    grid_size = _grid_size;
    pegs = _pegs;
    legal_moves = _legal_moves;
    moves_to_win = _moves_to_win;
    
    moves = _moves;
    dependent = _dependent;
    printf("Setting up:\npegs = %d, legal_moves = %d, grid_size = %d, moves_to_win = %d, old = %d\n", pegs, legal_moves, grid_size, moves_to_win, old);
    
	return 0;
}


int solve(int state[grid_size][grid_size], const int win_board[grid_size][grid_size], int move_hist[moves_to_win], int Kill[1]){
    
    printf("Solving; excluding move combinations:\n");
    short are_dependent_m[legal_moves][legal_moves];
    int _dependent_len[legal_moves];
    dependent_len = _dependent_len;
    compile_dependent(dependent, are_dependent_m);
    
    #if old == 0
    list *_move_list[moves_to_win];
    list _move_list_array[legal_moves];
    list * next_pointers[legal_moves][moves_to_win];
    move_list = _move_list;
    move_list_array = _move_list_array;
    
    init_move_list(0, state, next_pointers);
    #endif
    
    move_num = 0;// The number of moves which have been performed (not counting moves which have been undone/backtracked)
    int printed_lines = 0, backtrack_count = 1; //counts number of lines which have been printed to terminal.
    int print_threshold = pegs - 20; // lower means fewer lines will be printed to terminal.
    int found = 0;
    int q_0 = 0; // in each step we start the search at move number q_0 (< legal_moves)
    int q_p = 0;
    int last_move=-1;// the last move which was performed (-1 means no move has been performed yet)
    
    /*int start = 0;
    int start_moves[100] = {7, 13, 0, 4, 21, 11, 24, 29, 34, 0, 30, 34, 42, 3, 45, 42, 67, 46, 52, 5, 33, 60, 8, 65, 55, 74, 40, 71, 74, 63}; // Start the search at a specific set of moves 
    //print_state(state);
    
    for( int i= 0; i<start; i++){
        move(start_moves[i], state, move_hist);
        last_move = start_moves[i];
        
    }*/
    
    #if old == 0
    list *entry = move_list[move_num];
    #endif  
    
    while (1){
        if(move_num == moves_to_win){
            if(not_done(state, win_board)){
                //the following will make the code thing there are no legal moves, thus it will backtrack
                #if old
                q_0 = legal_moves;
                #else
                entry = NULL;
                #endif
            }else{
                //We have found a solution so we break the loop.
                break;
            
            }
        }
        //printf("loop<\n");
        //c--;
        found = 0;
        //__asm__("int $3");
        //counter ++;
        
        
        #if old
        if (q_0 < last_move){ 
            for(int p=0; p<28; p++){
                q_p = dependent[last_move][p];
                
                if( q_p < q_0){
                    continue;
                }
                if(  last_move <= q_p ){
                    break;
                }
                if(can_move(q_p, state)){
                    move(q_p, state, move_hist);
                    
                    last_move = q_p;
                    found = 1;
                    break;
                }
            }
            if(found){
                q_0 = 0;
                continue;
            }
            q_0 = last_move+1;
            
        }
        
        
        
        for(int q=q_0; q<legal_moves; q++){
            if(can_move(q, state) && not_a_permutation(q, move_hist, are_dependent_m)){
                move(q, state, move_hist);
                //update_move_list(q, dependent);
                last_move = q;
                found = 1;
                break;
            }
        }
        
        #else
        
        for(list *ptr = entry; ptr != NULL;  ptr = ptr->next[move_num]){
            //printf("q = %d", (*ptr)->move);
            if(not_a_permutation( ptr->move, move_hist, are_dependent_m)){
                move(ptr->move, state, move_hist);
                found = 1;
                break;
            }
        }
        
        #endif
        
        if(found){
            //printf("move_num = %d\n", move_num);
            
            #if old
            q_0 = 0;
            #else
            entry = move_list[move_num];
            #endif 
            continue;
        }else{
            // No further moves are allowed with the current board state.
            if(move_num == 0){
                // There are no more moves to backtrack, thus all possible moves have been searched i.e. a solution doesn't exist. 
                printf("seached through all posibillities, no solution found\n");
                return 1;
            }else{
                // We undo the last made move.
                q_0 = move_hist[move_num-1];
                
                #if general
                if( Kill[0] == 1){
                    return 2;
                }
                backtrack_count ++;
                #endif
                if(move_num < print_threshold){
                    for(int i = 0; i < move_num; i++){
                        printf("%2d ", move_hist[i]);
                    }
                    for(int i = move_num; i < print_threshold-1; i++){
                        printf("-- ");
                    }
                    printf("\n");
                    #if general
                    printed_lines ++;
                    if(backtrack_count/printed_lines < 3000000 && backtrack_count > 3000000){
                        
                        print_threshold --;
                        backtrack_count =0;
                        printed_lines =0;
                    }else if(backtrack_count/printed_lines > 30000000){
                        print_threshold ++;
                        backtrack_count =0;
                        printed_lines =0;
                    }
                    #endif 
                }
                
                
                back_track(q_0, state);
                #if old
                q_0++;
                if(move_num != 0){
                    // move_num has changed so last_move != q0
                    last_move = move_hist[move_num-1];
                }else{
                    last_move = -1;
                }
                #else
                entry = move_list_array[q_0].next[move_num];
                #endif
            }
        }
        
        
    }
    
    printf("\nEnd result:\n");
    for(int i = 0; i < move_num; i++){
        printf("%d ", move_hist[i]);
    }
    printf("\n");
    
    //memcpy(state_, state, sizeof(int)*grid_size*grid_size);  
    
    return 0;
}


void compile_moves(int board[grid_size][grid_size], int moves_[legal_moves][6], int dependent_[legal_moves][28]){
    
    
    const int dv[4][2] = {{1,0},{0,1}, {-1,0}, {0,-1}};
    int peg = 0;
    int move = 0;
    
    for( int i = 0; i < grid_size; i++){
        for( int j = 0; j < grid_size; j++){
            
            if( board[i][j] != 1){
                continue;
            }
            
            for( int d = 0; d < 4 ; d++){
                // Will the move take go outside the board grid?
                if( grid_size <= i + dv[d][0]*2 || i + dv[d][0]*2 < 0 ||  grid_size <= j + dv[d][1]*2 || j + dv[d][1]*2 < 0 ){
                    continue;
                }
                
                // Check if the two other peg holes are on the board.
                if( board[i+ dv[d][0]][j+ dv[d][1]] != 1 || board[i+ 2*dv[d][0]][j+ 2*dv[d][1]] != 1){
                continue;
                }
                
                moves_[move][0] = i;
                moves_[move][1] = j;
                moves_[move][2] = i +   dv[d][0];
                moves_[move][3] = j +   dv[d][1];
                moves_[move][4] = i + 2*dv[d][0];
                moves_[move][5] = j + 2*dv[d][1];
                move ++;
            }
            
            peg ++;
        }
    }
    
    if( move != legal_moves){
        printf("Error move != legal_moves (%d != %d)", move, legal_moves);
    
    }
    
    int c;
    int found = 0; 
    for(int q=0; q<legal_moves; q++){
        c = 0;
        for(int p = 0; p < 28; p ++){
            dependent_[q][p] = legal_moves;
        }
        
        for(int p = 0; p < legal_moves; p ++){
            found = 0;
            for( int n = 0; n<3; n++){
                for( int m = 0; m<3; m++){
                    if( (moves_[q][n*2+0] == moves_[p][m*2+0]) && (moves_[q][n*2+1] == moves_[p][m*2+1]) ){
                        dependent_[q][c] = p;
                        c ++;
                        found  = 1;
                        break;
                    }
                }
                if(found){
                    break;
                }
            }
        }
    }
}


int main(){
    
    legal_moves  = 76;
    pegs  = 32;
    grid_size =  7;
    moves_to_win = pegs-1;

    int state_[7][7] =    {                 {-1,-1,1,1,1,-1,-1},
                                            {-1,-1,1,1,1,-1,-1},
                                            { 1, 1,1,1,1, 1, 1},
                                            { 1, 1,1,0,1, 1, 1},
                                            { 1, 1,1,1,1, 1, 1},
                                            {-1,-1,1,1,1,-1,-1},
                                            {-1,-1,1,1,1,-1,-1}};
                                            
    
    /*int state_[grid_size][grid_size] =    { {0,0,0,0,0,0,0},
                                            {0,0,0,1,0,0,0},
                                            {0,0,0,1,0,0,0},
                                            {0,0,0,0,0,0,0},
                                            {0,0,0,0,0,0,0},
                                            {0,0,0,0,0,0,0},
                                            {0,0,0,0,0,0,0}};*/
    
    
    
                   
    int board[7][7] =    {                  {0,0,1,1,1,0,0},
                                            {0,0,1,1,1,0,0},
                                            {1,1,1,1,1,1,1},
                                            {1,1,1,1,1,1,1},
                                            {1,1,1,1,1,1,1},
                                            {0,0,1,1,1,0,0},
                                            {0,0,1,1,1,0,0}};
                                        
                                        
    int win_board_[7][7] = {                {-1,-1,0,0,0,-1,-1},
                                            {-1,-1,0,0,0,-1,-1},
                                            { 0, 0,0,0,0, 0, 0},
                                            { 0, 0,0,1,0, 0, 0},
                                            { 0, 0,0,0,0, 0, 0},
                                            {-1,-1,0,0,0,-1,-1},
                                            {-1,-1,0,0,0,-1,-1}};
    int Kill_[1] = {0};
    
    int moves_[76][6], dependent_[76][28], move_hist_[moves_to_win];
    
    compile_moves(board, moves_, dependent_);
    
    
    set_up_globals(7, 32, 76, 31, moves_, dependent_);
    printf("moves[7] = [%d, %d, %d, %d, %d, %d]\n", moves[7][0], moves[7][1], moves[7][2], moves[7][3], moves[7][4], moves[7][5]);
    printf("can_move(7, state) = %d\n", can_move(7, state_));
    
    solve(state_, win_board_, move_hist_, Kill_);
    






}
