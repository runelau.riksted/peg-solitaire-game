# Peg Solitaire Solver

This project is a GUI based solver and analyzer for a generalized version of the game "Peg solitaire" a description of the game can be found here: https://en.wikipedia.org/wiki/Peg_solitaire.

The project provides a GUI, where one can setup the game board. The GUI allows one to add or remove holes from the game board and then to choose where the pegs are placed at the beginning of the game, and where they should be after solving the game.

One of the goals of the project was to demonstrate the advantages of mixing compiled C code and python scripting. The GUI, the game setup and initial analysis of the game setup are performed in Python. This allows flexibility and ease of programming at the price of execution speed. Solving the game by exhaustive search would be almost futile with a python scrip, since it is simply not fast enough to search the immense possibility space. Therefore the Python script only sets up the game state and performs an initial analysis of it. This is then passed on (as pointers) to the core search algorithm, which is implemented in C. The result of this search is then read out by the Python script, which simply displays the solution or lack there of through the GUI.

The compiled C library is created by having the Python script call the GCC compiler, to compile the C code into a shared object. This shared object is then linked into the Python interpreter using the Ctypes library. If the GCC compiler isn't available, then the script will try to load a precompiled generalized version if one is available (e.g. under Windows).


In the end this mixed approach, allows one to get the performance of C, where it is most needed and the flexibility of Python for all the other parts. The current implementation easily solves the "English version", with 32 pegs, but for a setup with ~25 pegs, it can take more than an hour to search through all possible move combinations. The time to search all possible move combinations depends roughly exponentially on the number of pegs. Thus one needs a certain amount of luck in order to find solutions for games with many pegs.

# How to use
Bellow is a picture of how the GUI looks. The solving algorithm, will try to find a set of moves, that will turn the board on the left into the board shown on the right.

To set up the starting and ending boards, start by setting the board size - that is how many rows and columns of holes and or pegs there should be. After inputting the desired value press ENTER.

To change the starting and ending boards, simply left click a peg/hole to add or remove a peg there. To turn a peg or a hole into a blank field simply right click it and vice versa. 


![alt text](/Graphics/Peg solitaire setup.png "The setup screen, where you can choose your start and ending positions.")

To make the solver try to find a set of moves, which turns the left board state into the board state on the right, simply press "Solve". If and When a solution is found, the GUI will present a move by move guide on how to solve the game as seen bellow.

![alt text](/Graphics/Peg solitaire solution walk through.png "After a solution is found, you can walk through the moves of the solution using the arrows on the bottom (or left and right on the keyboard).")

Please notice, that finding a solution can take a very long time if there are more than ~ 20 pegs on the board.


# How to setup
Either make a Git clone of the repository or simply download the project folder and unpack it.
The only non standard Python package needed is numpy.
you can install it by typing "pip3 install -r requirements.txt" or "pip install -r requirements.txt" into the console when in the project folder.

# How to run
The software is run by running "peg.py" with the python 3 interpreter. (i.e. typing "python peg.py"  or "python3 peg.py" into a console after changing directory to the peg-solitaire-game folder in set folder.

# Requirements
The project requires Python 3 to run and should run on Windows and on Linux.
The project requires the following Python packages:
.1 Numpy

You can install it by typing "pip3 install -r requirements.txt" or "pip install -r requirements.txt" into the console when in the project folder.


