import re
# This code in this file was adapted from:
#https://stackoverflow.com/questions/41315873/attempting-to-resolve-blurred-tkinter-text-scaling-on-windows-10-high-dpi-disp#43046744

def get_HWND_DPI(window_handle):

    #To detect high DPI displays and avoid need to set Windows compatibility flags
    import os
    if os.name == "nt":
        from ctypes import windll, pointer, wintypes
        try:
            windll.shcore.SetProcessDpiAwareness(1)
        except Exception:
            pass  # this will fail on Windows Server and maybe early Windows
        DPI100pc = 96  # DPI 96 is 100% scaling
        DPI_type = 0  # MDT_EFFECTIVE_DPI = 0, MDT_ANGULAR_DPI = 1, MDT_RAW_DPI = 2
        winH = wintypes.HWND(window_handle)
        monitor_handle = windll.user32.MonitorFromWindow(winH, wintypes.DWORD(2))  # MONITOR_DEFAULTTONEAREST = 2
        X = wintypes.UINT()
        Y = wintypes.UINT()
        try:
            windll.shcore.GetDpiForMonitor(monitor_handle, DPI_type, pointer(X), pointer(Y))
            return X.value, Y.value, (X.value + Y.value) / (2 * DPI100pc)
        except Exception:
            return 96, 96, 1  # Assume standard Windows DPI & scaling
    else:
        return None, None, 1  # What to do for other OSs?


def Tk_geometry_scale(old_string, dpi_scale):

    patt = r"(?P<W>\d+)x(?P<H>\d+)(?:\+(?P<X>\d+)\+(?P<Y>\d+))?"  # format "WxH(+X+Y)"
    match = re.compile(patt).search(old_string)
    if match:
        new_string = str(dpi_scale(match.group("W")))
        new_string += "x" + str(dpi_scale(match.group("H")))

        if match.group("X") and match.group("Y"):
            # Add window position if present
            new_string += "+" + str(dpi_scale(match.group("X")))
            new_string += "+" + str(dpi_scale(match.group("Y")))
        return new_string
    else:
        raise ValueError('input must be "WxH+X+Y" or "WxH" with W, H, X & Y must be numbers expressed as strings.')


def make_Tk_DPI_aware(TKGUI):
    TKGUI.DPI_X, TKGUI.DPI_Y, TKGUI.DPI_scaling = get_HWND_DPI(TKGUI.winfo_id())
    TKGUI.DPI_scale = lambda v: round(float(v) * TKGUI.DPI_scaling)
    TKGUI.DPI_geometry = lambda s: Tk_geometry_scale(s, TKGUI.DPI_scale)
