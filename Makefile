
CFlags = -shared -fPIC -o

CC32 = gcc -m32
CC64 = gcc
CCWin32 = i686-w64-mingw32-gcc
CCWin64 = x86_64-w64-mingw32-gcc


all:
	$(CC32)    $(CFlags) Fallback_binaries/peg_Linux_32bit.so peg_general.c
	
	$(CC64)    $(CFlags) Fallback_binaries/peg_Linux_64bit.so peg_general.c
	
	$(CCWin32) $(CFlags) Fallback_binaries/peg_Windows_32bit.so peg_general.c
	
	$(CCWin64) $(CFlags) Fallback_binaries/peg_Windows_64bit.so peg_general.c

32:
	$(CC32)    $(CFlags) Fallback_binaries/peg_Linux_32bit.so peg_general.c
	
64:
	$(CC64)    $(CFlags) Fallback_binaries/peg_Linux_64bit.so peg_general.c
	
Win32:
	$(CCWin32) $(CFlags) Fallback_binaries/peg_Windows_32bit.so peg_general.c
	
Win64:
	$(CCWin64) $(CFlags) Fallback_binaries/peg_Windows_64bit.so peg_general.c
	
